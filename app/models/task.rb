class Task < ActiveRecord::Base
  belongs_to :user
  has_many :sub_tasks, class_name: "Task",
                          foreign_key: "parent_id"
 
  belongs_to :parent, class_name: "Task"

  validates_presence_of :name, :duration, :user_id

  searchable do
    text :name
    integer :duration
  end

  scope :recent, -> { order(updated_at: :desc) }

end

