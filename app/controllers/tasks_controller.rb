class TasksController < ApplicationController
  require 'will_paginate/array'
  load_and_authorize_resource

  before_action :authenticate_user!
  before_action :get_recent

  respond_to :js
  
  def index
    @search = Sunspot.search(Task) {
      fulltext params[:search_name]
      all_of do
        with(:duration).less_than(params[:search_less]) if !params[:search_less].blank?
        with(:duration).greater_than(params[:search_more]) if !params[:search_more].blank?
      end
    } 
    @tasks = @search.results
    @tasks = @tasks.paginate(:page => params[:page], per_page: 10)
  end

  def show
  end

  def new
    @task = Task.new
  end

  def edit
  end

  def create
    @task = Task.new(task_params)
    @task.user_id ||= current_user.id

    redirect_to :back if !@task.save
  end

  def update
    redirect_to :back if !@task.update(task_params)
  end

  def destroy
    @task.destroy
  end

  private
    def get_recent
      @tasks = Task.recent.paginate(:page => params[:page], per_page: 10)
    end

    def task_params
      params.require(:task).permit(:name, :duration, :user_id, :search_name, :search_less,
        :search_more)
    end
end
