class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.integer :duration
      t.references :user, index: true
      t.references :parent, index: true

      t.timestamps null: false
    end
    add_foreign_key :tasks, :users
  end
end
